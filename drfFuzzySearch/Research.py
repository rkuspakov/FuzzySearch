from fuzzywuzzy import fuzz
import sqlite3 as sl
import re
from urllib.parse import unquote


class Research:

    def find_word(self, rat, name):
        name = name.split("q=")
        name = name[1].replace("'>", "")
        Name = unquote(name, 'utf-8')
        print("Наименование:" + Name)
        print("К:" + str(rat))
        # Name = "Доска классная"
        con = sl.connect(r'MatchingSearch.db')
        cursor = con.cursor()

        cursor.execute("SELECT Name FROM Perechen")
        massiveS = cursor.fetchall()

        Word = Name.replace("'", "")
        Word = Word.lower()
        Word = re.sub("[0-9]", "", Word)
        Word = re.sub('\W+', ' ', Word)

        res_list = []
        for i in range(len(massiveS)):
            if massiveS[i][0] not in res_list:
                res_list.append(massiveS[i][0])

        queryset = []
        for item in res_list:

            rat1 = fuzz.ratio(item.lower(), Word.lower())
            rat2 = fuzz.partial_ratio(item.lower(), Word.lower())
            rat3 = fuzz.token_sort_ratio(item.lower(), Word.lower())
            rat4 = fuzz.token_set_ratio(item.lower(), Word.lower())

            rat = int(rat)
            if rat1 > rat or rat2 > rat or rat3 > rat or rat4 > rat:
                  queryset.append("{ 'Name' : '" + Name  + "', 'Word' : '" + item  + "', 'rat1' : '" + str(rat1)  +
                                  "', 'rat2' : '" + str(rat2)  + "', 'rat3' : '" + str(rat3)  + "', 'rat4' : '" + str(rat4) + "' }")

        return str(queryset)