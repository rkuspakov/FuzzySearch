from django.apps import AppConfig


class DrfFuzzySearchConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'drfFuzzySearch'
